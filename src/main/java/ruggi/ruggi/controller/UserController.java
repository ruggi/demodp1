package ruggi.ruggi.controller;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ruggi.ruggi.business.UserBusiness;
import ruggi.ruggi.exceptions.UserCredentialsNotExist;
import ruggi.ruggi.model.UserModel;
import ruggi.ruggi.services.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserModel createUser(@RequestBody String bodyTxt) {

        JsonObject body = new JsonParser().parse(bodyTxt).getAsJsonObject();
        String lastname = body.get("lastname").getAsString();
        String name = body.get("name").getAsString();
        String code = body.get("code").getAsString();
        String password = new UserBusiness().CreatePassword();

        UserModel userResponse = new UserModel();
        userResponse.setCode(Integer.parseInt(code));
        userResponse.setLastname(lastname);
        userResponse.setName(name);
        userResponse.setPassword(password);

        return userService.save(userResponse);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserModel> listUser() {
        return userService.findAllUser();
    }

    @RequestMapping(value = "/list2", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserModel> listUser2() {
        return userService.getAllUser();
    }


    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserModel authUser(@RequestBody String bodyTxt) {

        JsonObject body = new JsonParser().parse(bodyTxt).getAsJsonObject();
        String user = body.get("user").getAsString();
        String password = body.get("password").getAsString();

        UserModel userModel = userService.findByNameAndPassword(user,password);
        if (userModel == null) { throw new UserCredentialsNotExist(user,password);}
        return userModel;
    }

}