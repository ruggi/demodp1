package ruggi.ruggi.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootController {

    @RequestMapping("/")
    public String greeting() {
        String welcome= "welcome";
        return welcome;
    }
}
