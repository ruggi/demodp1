package ruggi.ruggi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuggiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuggiApplication.class, args);
    }

}
