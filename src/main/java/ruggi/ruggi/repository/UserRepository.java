package ruggi.ruggi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ruggi.ruggi.model.UserModel;

import java.util.ArrayList;

@Repository
public interface UserRepository  extends JpaRepository<UserModel, Integer> {
    @Query(value = "select * from userdp2 ", nativeQuery = true)
    ArrayList<UserModel> getAllUser();
    UserModel findByNameAndPassword(String name,String password);
    UserModel findById(String id);
}
