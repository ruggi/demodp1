package ruggi.ruggi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String id) {
        super(String.format("El usuario con el %s no existe.", id));

    }
}
