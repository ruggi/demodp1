package ruggi.ruggi.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ruggi.ruggi.model.UserModel;
import ruggi.ruggi.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;
    @Autowired
    public UserService(UserRepository userRepository){this.userRepository = userRepository;}

    public List<UserModel> findAllUser(){
        return userRepository.findAll();
    }
    public ArrayList<UserModel> getAllUser(){return userRepository.getAllUser();}
    public void delete(UserModel userModel){
        userRepository.delete(userModel);
    }
    public UserModel save( UserModel userModel){ return userRepository.save(userModel);}
    public UserModel findByNameAndPassword(String name,String password){return userRepository.findByNameAndPassword(name,password);}
    public UserModel findById(String id){return userRepository.findById(id);}
}
